#!/bin/bash

CP=$(pwd)
SCRIPT_PATH="${CP}/modify.sh"

mkdir "tmp_examples"

cd "tmp_examples"

clear

printf "This is an instructional script presenting some of the basic functionalities of the program 'modify.sh'\nPress 'y' to continue or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 cd ..
 rm -r "tmp_examples"
 exit 0
fi

printf "Scenario no 1: Uppercasing the filename: file1.tar.gz\n"

#sleep 2

touch "file1.tar.gz"

printf "The structure of the folder"

ls -R

"$SCRIPT_PATH" "-u" "file1.tar.gz"

printf "Running the command: ${CP}/script.sh -u file1.tar.gz"
printf "\n"

printf "The structure of the folder after the changes"

ls -R

printf "Press 'y' to continue to the next scenario or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 #rm -r "tmp_examples"
 exit 0
fi

clear

printf "Scenario no 2: Lowercasing the filename: FILE1.tar.gz\n"

#sleep 2

printf "The structure of the folder"

ls -R

printf "Running the command: ${CP}/script.sh -u FILE1.tar.gz"
printf "\n"

"$SCRIPT_PATH" "-l" "FILE1.tar.gz"

printf "The structure of the folder after the changes"

ls -R

printf "Press 'y' to continue to the next scenario or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 cd ..
 rm -r "tmp_examples"
 exit 0
fi

clear

printf "Scenario no 3: An attempt to uppercase an improper filename: file1 .tar.gz\n"

touch "file1 .tar.gz"

#sleep 2

printf "The structure of the folder"

ls -R

printf "Running the command: ${CP}/script.sh -u file1 .tar.gz"
printf "\n"

"$SCRIPT_PATH" "-l" "file1 .tar.gz"

printf "The structure of the folder after the changes"

ls -R

printf "Press 'y' to continue to the next scenario or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 cd ..
 rm -r "tmp_examples"
 exit 0
fi

clear

printf "Scenario no 4: Recursive lowercasing in the current directory\n"

rm "file1 .tar.gz"
touch "FiLE2.txt"
touch "FIle3.txt"
mkdir "tmp_examples1"
cd "tmp_examples1"
touch "filE4.tar.gz"
touch "FILE5.txt"
cd ..
mkdir "tmp_examples2"
cd "tmp_examples2"
touch "file6.txt"
touch "FILE7.tar.gz"
cd ..

#sleep 2

printf "The structure of the folder"

ls -R

printf "Running the command: ${CP}/script.sh -r -l tmp_examples"
printf "\n"

cd ..

"$SCRIPT_PATH" "-r" "-l" "tmp_examples"

cd "tmp_examples"

printf "The structure of the folder after the changes"

ls -R

printf "Press 'y' to continue to the next scenario or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 cd ..
 rm -r "tmp_examples"
 exit 0
fi

clear

printf "Scenario no 5: Using sed pattern: 's/fi/bi/g' recursively in the current directory\n"

#sleep 2

printf "The structure of the folder"

ls -R

printf "Running the command: ${CP}/script.sh -r 's/fi/bi/g' tmp_examples"
printf "\n"

cd ..

"$SCRIPT_PATH" "-r" "'s/fi/bi/g'" "tmp_examples"

cd "tmp_examples"

printf "The structure of the folder after the changes"

ls -R

printf "Press 'y' to continue to the next scenario or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 cd ..
 rm -r "tmp_examples"
 exit 0
fi

clear
printf "Scenario no 6: Using the help command\n"

#sleep 2

printf "Running the command: ${CP}/script.sh -h"
printf "\n"

"$SCRIPT_PATH" "-h"

printf "Press 'y' to continue to the next scenario or 'n' to finish"

read yn

if [ "$yn" = "n" ] || [ "$yn" = "N" ]
then
 cd ..
 rm -r "tmp_examples"
 exit 0
fi

clear
printf "Scenario no 7: Modifying series of files\n"

#sleep 2

printf "The structure of the folder"

ls -R

printf "Running the command: ${CP}/script.sh -r -u tmp_examples1 tmp_examples2"
printf "\n"

"$SCRIPT_PATH" "-r" "-u" "tmp_examples1" "tmp_examples2"

printf "The structure of the folder after the changes"

ls -R

printf "End of the tesing script. Press any key to finish."

read yn

clear

cd ..
rm -r "tmp_examples"
exit 0
