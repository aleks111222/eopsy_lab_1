#!/bin/bash

Rename() {

 Arg="$1"
 FullName="$2"

 if [[ "$FullName" =~ \ |\' ]] # if a space appears in the name of the file / directory / path
 then
  printf "modify: Improper filename\n"
  exit 1
 fi

 filename="${FullName##*/}" # basename

 if [[ "$filename" == *"."* ]] # checking if file has an extention
 then
  extension=".${filename#*.}"
 else
  extension=""
 fi

 filename="${filename%%.*}" # filename without extension

 path="$(dirname ${FullName})"

 if [ "$Arg" = "-u" ]
 then
  NewFilename=${filename^^}
 elif [ "$Arg" = "-l" ]
 then
  NewFilename=${filename,,}
 else
  Sed="$(echo $Arg | sed "s/'//g")"
  NewFilename="$(echo $filename | sed "$Sed")"
 fi

 if [ ! "$filename" = "$NewFilename" ] # if any change happened
 then
  mv "${FullName}" "${path}/${NewFilename}${extension}"
 fi
}

if [ "$1" = "-h" ] || [ "$1" = "-help" ]
then
 printf "Usage: modify [OPTION] [FILE]\n  or:  modify [-r] [OPTION] [DIRECTORY]\n  or:  modify [-r] {SED} [DIRECTORY]\nUppercasing or lowercasing filenames.\n\n"
 printf "Option:\n  -h  -help  shows the command's usage description\n  -l         changes the name of a specified file to lowercase\n  -u         changes the name of a specified file to uppercase\n"
 printf "  -r         changes all filenames of a specified directory and its subdirectories according to the rule indicated by a following [OPTION] or a {SED} pattern.\n\n"
 printf "Exit status:\n 0  if OK,\n 1  if minor problems (e.g., syntax misuse),\n 2  if serious issue.\n"
 exit 0
fi

if ([ ! "$1" = "-r" ] && [ ! "$1" = "-u" ] && [ ! "$1" = "-l" ] && [ ! "$1" = "-h" ]) # checking syntax
then
 printf "modify: unrecognised option.\nTry 'modify -h' for more information.\n"
 exit 1
fi

array=( "$@" )
arraylength="${#array[@]}" # array of arguments

if [ "$1" = "-r" ]
then
 CurrPath=$(pwd)

 for (( i=2; i<"${arraylength}"; i++ ));
 do
    if [ ! -d "${array[$i]}" ] # check if path points to a directory
     then
     printf "modify: missing directory\n"
     exit 1
    fi
    find "${array[$i]}" -type f | while read Name; do
     Rename "$2" "$Name" # iterate through each file and change its name
    done
 done
 exit 0

else
  for (( i=1; i<"${arraylength}"; i++ ));
  do
    if [ ! -f "${array[$i]}" ] # check if the path indicates a file
     then
     printf "modify: missing file\n"
     exit 1
    fi
    Rename "$1" "${array[$i]}"
  done
 exit 0
fi

exit 2
