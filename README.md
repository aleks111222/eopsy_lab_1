Simple bash script that modifies file names in a recursive manner.
Syntax:
modify [-r] [-l|-u] <dir/file names...>   
modify [-r] <sed pattern> <dir/file names...>   
modify [-h]
Run modify_examples.sh within the directory containting modify.sh to be presented with an instructional program.
